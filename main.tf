data "terraform_remote_state" "example" {
  backend = "http"

  config = {
    address = "https://gitlab.com/api/v4/projects/21699354/terraform/state/production"
  }
}

data "aws_eks_cluster" "my-cluster" {
  name = data.terraform_remote_state.example.outputs.cluster_id
}

data "aws_eks_cluster_auth" "my-auth" {
  name = data.terraform_remote_state.example.outputs.cluster_id
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.my-cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.my-cluster.certificate_authority.0.data)
    token                  = data.aws_eks_cluster_auth.my-auth.token
    load_config_file       = false
  }
}
provider "kubernetes" {
  host                   = data.aws_eks_cluster.my-cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.my-cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.my-auth.token
  load_config_file       = false
}


provider "rancher2" {
  alias     = "bootstrap"
  api_url   = "https://${var.rancher_hostname}"
  bootstrap = true
}


module "install_rancher" {
  source           = "./modules/install-rancher"
  rancher_hostname = var.rancher_hostname
  rancher_password = var.rancher_password

  providers = {
    helm               = helm
    kubernetes         = kubernetes
    rancher2.bootstrap = rancher2.bootstrap
  }


  rancher_sets = [
    {
      "name"  = "ingress.tls.source",
      "value" = "letsEncrypt",
      "type"  = "auto",
    },
    {
      "name"  = "letsEncrypt.email",
      "value" = "emccudden@gmail.com",
      "type"  = "auto",
    },
    {
      "name"  = "replicas",
      "value" = "1",
      "type"  = "auto",
    },
  ]
}
